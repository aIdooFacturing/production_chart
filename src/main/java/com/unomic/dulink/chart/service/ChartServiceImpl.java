package com.unomic.dulink.chart.service;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.codehaus.jackson.map.ObjectMapper;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.unomic.dulink.chart.domain.ChartVo;
 

@Service
@Repository
public class ChartServiceImpl extends SqlSessionDaoSupport implements ChartService{
	private final static String namespace= "com.unomic.dulink.chart.";
	
	@Override
	public String getStartTime(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String rtn = (String) sql.selectOne(namespace + "getStartTime", chartVo);
		return rtn;
	}
	
	@Override
	public String getComName(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = URLEncoder.encode((String) sql.selectOne(namespace + "getComName", chartVo),"utf-8");
		return str;
	}
	
	
	@Override
	public ChartVo getBanner(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		chartVo = (ChartVo) sql.selectOne(namespace + "getBanner", chartVo);
		return chartVo;
	}

	@Override
	public String login(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = "";
		
		int exist = (int) sql.selectOne(namespace + "login", chartVo);
		
		if(exist==0){
			str = "fail";
		}else{
			str = "success";
		};
		
		return str;
	};
	
	//common func
	
	
	@Override
	public String getJigList4Report(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List <ChartVo >dvcList = sql.selectList(namespace + "getJigList4Report", chartVo);
		
		List list = new ArrayList();
		for(int i = 0; i < dvcList.size(); i++){
			Map map = new HashMap();
			map.put("name", URLEncoder.encode(dvcList.get(i).getName(), "utf-8"));
			map.put("dvcId", URLEncoder.encode(dvcList.get(i).getName(), "utf-8"));
			list.add(map);
		};
		
		Map dvcMap = new HashMap();
		dvcMap.put("dvcList", list);
		
		String str = "";
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dvcMap);
		
		return str;
	}
	
	@Override
	public String getProductionCnt(ChartVo chartVo) throws Exception {
		
		SqlSession sql = getSqlSession();
		List <ChartVo> wcList = sql.selectList(namespace + "getProductionCnt", chartVo);
		
		List list = new ArrayList();
		
		for(int i = 0; i < wcList.size(); i++){
			Map map = new HashMap();
			
			map.put("dvcId", wcList.get(i).getDvcId());
			map.put("name", URLEncoder.encode(wcList.get(i).getName(), "utf-8"));
//			map.put("tgCyl", i*100);
			map.put("tgCyl", wcList.get(i).getTgCyl());
//			map.put("cntCyl", i*10);
			map.put("cntCyl", wcList.get(i).getCntCyl());
			
			list.add(map);
		};
		
		
		Map dataMap = new HashMap();
		dataMap.put("wcList", list);
		
		String str = "";
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

};